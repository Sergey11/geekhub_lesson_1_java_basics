package com.geekhub.lesson8;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ImageCrawler downloads all images to specified folder from specified resource.
 * It uses multi threading to make process faster. To start download images you should call downloadImages(String urlToPage) method with URL.
 * To shutdown the service you should call stop() method
 */
public class ImageCrawler {

    //number of threads to download images simultaneously
    public static final int NUMBER_OF_THREADS = 10;

    private ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private String folder;

    public ImageCrawler(String folder) throws MalformedURLException {
        this.folder = folder;
    }

    /**
     * Call this method to start download images from specified URL.
     *
     * @param urlToPage
     * @throws IOException
     */
    public void downloadImages(String urlToPage) throws IOException {
        //implement me
        URL url = new URL(urlToPage);
        Page page = new Page(url);
        try {
            for (URL imageURL : page.getImageLinks()) {
                if (isImageURL(imageURL)) {
                    executorService.submit(new ImageTask(imageURL, folder));
                }
            }
            for (URL linkURL : page.getLinks()) {
                if (isImageURL(linkURL)) {
                    executorService.submit(new ImageTask(linkURL, folder));
                }
            }
        } catch (MalformedURLException e) {
            System.out.println("help me");
        }
    }

    /**
     * Call this method before shutdown an application
     */
    public void stop() {
        executorService.shutdown();
    }

    //detects is current url is an image. Checking for popular extensions should be enough
    private boolean isImageURL(URL url) {
        //implement me
        Pattern pattern = Pattern.compile("([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)");
        Matcher matcher = pattern.matcher(url.toString());
        return matcher.matches();
    }
}
