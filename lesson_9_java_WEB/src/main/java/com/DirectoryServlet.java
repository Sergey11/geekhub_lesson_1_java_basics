package com;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;


@WebServlet("/file_manager/*")
public class DirectoryServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileSystem fileSystem = new FileSystem();
        String resultUrl = URLDecoder.decode(request.getRequestURI(), "UTF-8");
        resultUrl = resultUrl.substring(resultUrl.indexOf("///") + 3, resultUrl.length());
        if (resultUrl.endsWith(".txt")) {
            try (ServletOutputStream out = response.getOutputStream();
                 InputStream in = new BufferedInputStream(new FileInputStream(resultUrl))) {
                int ch;
                while ((ch = in.read()) != -1) {
                    out.print((char) ch);
                }
            }
            return;
        }
        request.setAttribute("AbsolutePath", resultUrl);
        request.setAttribute("PathDown", fileSystem.parentURI(resultUrl));
        request.setAttribute("directories", fileSystem.getURIs(resultUrl));
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/new.jsp");
        rd.forward(request, response);
    }
}
