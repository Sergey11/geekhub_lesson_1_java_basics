package com;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/file_manager")
public class MyServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FileSystem fileSystem = new FileSystem();

        request.setAttribute("directories", fileSystem.getRootURIs());
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/new.jsp");
        rd.forward(request, response);
    }
}