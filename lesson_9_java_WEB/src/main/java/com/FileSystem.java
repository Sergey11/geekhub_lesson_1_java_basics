package com;

import java.io.IOException;
import java.net.URI;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

public class FileSystem {

    private Iterable<Path> files(String path) throws IOException {
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(path))) {
            List<Path> result = new ArrayList<>();
            for (Path directoryPath : directoryStream) {
                result.add(directoryPath);
            }
            return result;
        }
    }

    public List<URI> getURIs(String path) throws IOException {
        List<URI> uriList = new ArrayList<>();
        for (Path p : files(path)) {
            uriList.add(p.toUri());
        }
        return uriList;
    }

    public URI parentURI(String path) {
        Path p = Paths.get(path).getParent();
        return p != null ? p.toUri() : null;
    }

    public List<URI> getRootURIs() {
        List<URI> uriList = new ArrayList<>();
        for (Path path : FileSystems.getDefault().getRootDirectories()) {
            if (path.toUri().toString() != null) {
                uriList.add(path.toUri());
            }
        }
        return uriList;
    }
}
