<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
    <a href="/file_manager"><strong>File manager</strong></a> ${AbsolutePath}<Br>
    <a href="${pageContext.request.contextPath}/file_manager/${PathDown}">${PathDown.getPath().substring(1)}</a><Br><Br>
    <c:forEach var="directory" items="${directories}">
        <a name="browser" href="${pageContext.request.contextPath}/file_manager/${directory}">
        ${directory.getPath().substring(1)}<Br>
    </c:forEach>
</body>
</html>