package com.geekhub.lesson5.source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        //TODO: implement me
        try {
            new URL(pathToSource);
        } catch (MalformedURLException e) {
            return false;
        }
        return true;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        //TODO: implement me
        URL url = new URL(pathToSource);
        try (InputStreamReader inputStreamReader = new InputStreamReader(url.openStream());
             BufferedReader in = new BufferedReader(inputStreamReader)) {
            String text = "";
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                text += inputLine;
            }
            return text;
        }
    }
}