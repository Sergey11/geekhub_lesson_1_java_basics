package com.geekhub.lesson5.source;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        //TODO: implement me
        return new File(pathToSource).exists();
    }

    @Override
    public String load(String pathToSource) throws IOException {
        //TODO: implement me

        try (FileReader fileReader = new FileReader(pathToSource);
             BufferedReader in = new BufferedReader(fileReader)) {
            String text = "";
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                text += inputLine;
            }
            return text;
        }
    }
}