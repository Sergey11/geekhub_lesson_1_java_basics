package com.geekhub.lesson5;

import com.geekhub.lesson5.source.NoValidPathException;
import com.geekhub.lesson5.source.SourceLoader;
import com.geekhub.lesson5.source.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    //https://dl.dropboxusercontent.com/u/14434019/en.txt
    public static void main(String[] args) throws IOException {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while (!"exit".equals(command)) {
            //TODO: add exception handling here to let user know about it and ask him to enter another path to translation
            //      So, the only way to stop the application is to do that manually or type "exit"

            try {
                String source = sourceLoader.loadSource(command);
                String translation = translator.translate(source);
                System.out.println("Original: " + source);
                System.out.println("Translation: " + translation);
            } catch (NoValidPathException e) {
                System.out.println("No valid path");
            }
            command = scanner.next();
        }
    }
}