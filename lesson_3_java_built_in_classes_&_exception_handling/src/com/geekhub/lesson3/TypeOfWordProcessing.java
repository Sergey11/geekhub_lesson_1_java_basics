package com.geekhub.lesson3;

public enum TypeOfWordProcessing {
    CONCATENATION, STRING_BUILDER, STRING_BUFFER
}
