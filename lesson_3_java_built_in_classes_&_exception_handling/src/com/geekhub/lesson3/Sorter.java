package com.geekhub.lesson3;

import java.util.Arrays;

public class Sorter {

    public static Comparable[] sort(Comparable[] element) {
        Comparable[] comparables = element.clone();
        Arrays.sort(comparables);
        return comparables;
    }
}