package com.geekhub.lesson3;

import static java.lang.Math.abs;

public class Bear implements Comparable<Bear> {
    private String species;
    private int weight;

    public Bear(String species, int weight) {
        this.species = species;
        this.weight = weight;
    }

    public String getSpecies() {
        return species;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public int compareTo(Bear bear) {
        int result = (this.species).compareTo(bear.species);
        if (result != 0) {
            return result;
        } else {
            result = (this.weight) - bear.weight;
            return result != 0 ? result / abs(result) : 0;
        }
    }
}