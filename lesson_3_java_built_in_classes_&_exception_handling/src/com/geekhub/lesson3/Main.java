package com.geekhub.lesson3;

public class Main {
    public static void main(String[] args) {
        processBears();
    }

    public static void processBears() {
        initStringsProcessing();
        Bear[] bears = initBears();
        System.out.println("List of bears");
        printBears(bears);

        System.out.println("Sorted bears");
        Bear[] sortedBears = (Bear[]) Sorter.sort(bears);
        System.out.println();
        printBears(sortedBears);
    }

    public static Bear[] initBears() {
        Bear[] bears = new Bear[3];
        String species;
        int weight;
        ConsoleHandler consoleHandler = new ConsoleHandler();
        System.out.println("Enter \"q\" if you want exit");
        System.out.println("Create five bears");
        for (int i = 0; i < bears.length; i++) {
            printInputMessages("Enter species of the ", (i + 1));
            species = consoleHandler.checkLinesBySting();
            printInputMessages("Enter weight of the ", (i + 1));
            weight = consoleHandler.checkLinesByInt();
            bears[i] = new Bear(species, weight);
        }
        return bears;
    }

    private static void printBears(Bear[] bears) {
        StringBuilder sentence = new StringBuilder();
        for (Bear bear : bears) {
            sentence.append(bear.getSpecies());
            sentence.append(", ");
            sentence.append(bear.getWeight());
            System.out.println(sentence);
            sentence.setLength(0);
        }
    }

    private static void printInputMessages(String phrase, int word) {
        StringBuilder sentence = new StringBuilder();
        sentence.append(phrase);
        sentence.append(word);
        sentence.append(" bear");
        System.out.println(sentence);
        sentence.setLength(0);
    }

    public static void initStringsProcessing() {
        ConsoleHandler consoleHandler = new ConsoleHandler();
        WorkWithString workWithString = new WorkWithString();
        System.out.println("Enter \"q\" if you want exit");
        System.out.println("Enter the small word");
        workWithString.setSmallWord(consoleHandler.checkLinesBySting());
        System.out.println("Enter the number of multiplications");
        workWithString.setCount(consoleHandler.checkLinesByInt());
        System.out.println("String concatenation");
        launchWordProcessing(workWithString, TypeOfWordProcessing.CONCATENATION);
        System.out.println("String buffer");
        launchWordProcessing(workWithString, TypeOfWordProcessing.STRING_BUFFER);
        System.out.println("String builder");
        launchWordProcessing(workWithString, TypeOfWordProcessing.STRING_BUILDER);
        System.out.println();
    }

    private static void launchWordProcessing(WorkWithString workForString, TypeOfWordProcessing typeOfWordProcessing) {
        String text;
        long start = System.currentTimeMillis();
        switch (typeOfWordProcessing) {
            case CONCATENATION:
                text = workForString.multiplierString();
                break;
            case STRING_BUILDER:
                text = workForString.multiplierStringBuffer();
                break;
            default:
                text = workForString.multiplierStringBuilder();
                break;
        }
        long end = System.currentTimeMillis();
        System.out.println(text);
        System.out.println(end - start + " ms");
    }
}
