package com.geekhub.lesson3;

public class WorkWithString {

    private int count = 1000;

    private String something = "a";

    public void setSmallWord(String something) {
        this.something = something;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String multiplierString() {
        String result = "";
        for (int i = 0; i < count; i++) {
            result += something;
        }
        return result;
    }

    public String multiplierStringBuffer() {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < count; i++) {
            stringBuffer.append(something);
        }
        return stringBuffer.toString();
    }

    public String multiplierStringBuilder() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < count; i++) {
            stringBuilder.append(something);
        }
        return stringBuilder.toString();
    }
}
