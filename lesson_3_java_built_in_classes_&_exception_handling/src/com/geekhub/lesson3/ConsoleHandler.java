package com.geekhub.lesson3;

import java.util.Scanner;

/**
 * This class checks the validity of the value of the console.
 *
 * @author Created by Musienko Sergey on 31.10.14.
 */
public class ConsoleHandler {
    private Scanner in;

    public ConsoleHandler() {
        in = new Scanner(System.in);
    }

    /**
     * This method reads the console line and check for validity.
     *
     * @return returns the line of the console
     */
    public String checkLinesBySting() {
        String line = in.nextLine();
        if (line.equals("q")) {
            System.exit(0);
        }
        return line;
    }

    /**
     * This method reads the console line and check for validity.
     *
     * @return returns the number of the console
     */
    public int checkLinesByInt() {
        String line = in.nextLine();
        int number;
        if (line.equals("q")) {
            System.exit(0);
        }
        try {
            number = Integer.parseInt(line);
            if (number > 0) {
                return number;
            } else {
                throw new NumberFormatException();
            }
        } catch (NumberFormatException e) {
            System.out.println("You entered is not a natural number, try again");
            return checkLinesByInt();
        }
    }
}