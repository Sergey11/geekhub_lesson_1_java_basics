package com.geekhub.lesson3.modified_lesson2;

import java.util.Scanner;

/**
 * This class checks the validity of the value of the console.
 *
 * @author Created by Musienko Sergey on 24.10.14.
 */
public class ConsoleHandler {
    private Scanner in;

    public ConsoleHandler() {
        in = new Scanner(System.in);
    }

    /**
     * This method reads the console line and check for validity.
     *
     * @param validValues array of valid values
     * @return returns the word of the console
     */
    public String checkLinesBy(String[] validValues) {
        String line = in.nextLine();
        for (String validValue : validValues) {
            if (validValue.equals(line)) {
                return line;
            }
        }
        System.out.println("Try again");
        return checkLinesBy(validValues);
    }
}