package com.geekhub.lesson3.modified_lesson2;

import com.geekhub.lesson3.modified_lesson2.vehicles.BoatBoardComputer;
import com.geekhub.lesson3.modified_lesson2.vehicles.CarBoardComputer;

public class NewApp {
    public static InitializationActionVehicle init;

    public static void main(String[] args) {
        initVehicle();
    }

    public static void initVehicle() {
        String[] validValues = {"?", "q", "c", "b"};
        ConsoleHandler consoleHandler = new ConsoleHandler();
        System.out.println(viewAllCommands());
        switch (consoleHandler.checkLinesBy(validValues)) {
            case "?":
                initVehicle();
                break;
            case "q":
                System.exit(0);
                break;
            case "b":
                BoatBoardComputer boatBoardComputer = new BoatBoardComputer();
                init = new InitializationActionVehicle(boatBoardComputer, boatBoardComputer);
                System.out.println("Hello, I am a Boat");
                init.startConsole();
                break;
            case "c":
                CarBoardComputer carBoardComputer = new CarBoardComputer();
                init = new InitializationActionVehicle(carBoardComputer, carBoardComputer);
                System.out.println("Hello, I am a Solar-powered car");
                init.startConsole();
                break;
        }
    }

    public static String viewAllCommands() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("? for view all commands,\n");
        stringBuilder.append("q for exit, \n");
        stringBuilder.append("c for create solar-powered car,\n");
        stringBuilder.append("b for create boat");
        return stringBuilder.toString();
    }
}
