package com.geekhub.lesson3.modified_lesson2.vehicles;

import com.geekhub.lesson3.modified_lesson2.engines.SpeedException;
import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;
import com.geekhub.lesson3.modified_lesson2.wheels.Direction;

public interface Drivable {
    void accelerate() throws FuelException, SpeedException;
    void brake() throws SpeedException, FuelException;
    void turn(Direction direction);
}
