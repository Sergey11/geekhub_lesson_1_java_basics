package com.geekhub.lesson3.modified_lesson2.vehicles;

import com.geekhub.lesson3.modified_lesson2.engines.Engine;
import com.geekhub.lesson3.modified_lesson2.engines.SpeedException;
import com.geekhub.lesson3.modified_lesson2.engines.TorqueMoment;
import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;
import com.geekhub.lesson3.modified_lesson2.gastanks.GasTank;
import com.geekhub.lesson3.modified_lesson2.wheels.Direction;
import com.geekhub.lesson3.modified_lesson2.wheels.Wheel;

public class SolarPoweredCar extends Vehicle {
    private Wheel[] wheels;

    public SolarPoweredCar(GasTank gasTank, Engine engine, Wheel[] wheels) {
        super(gasTank, engine);
        this.wheels = wheels;
    }

    @Override
    public void accelerate() throws SpeedException, FuelException {
        TorqueMoment torqueMoment = getEngine().getTorqueMoment();
        wheels[0].run(torqueMoment);
    }

    @Override
    public void brake() throws SpeedException, FuelException {
        TorqueMoment torqueMoment = getEngine().getTorqueMoment();
            wheels[0].stop(torqueMoment);
    }

    @Override
    public void turn(Direction direction) {
        wheels[0].turn(direction);
        wheels[1].turn(direction);
        if (direction == Direction.LEFT) {
            System.out.println("I turn left");
        } else {
            System.out.println("I turn rite");
        }
    }

}
