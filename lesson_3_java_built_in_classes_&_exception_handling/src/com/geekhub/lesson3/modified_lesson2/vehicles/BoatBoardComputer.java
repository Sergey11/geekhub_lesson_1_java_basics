package com.geekhub.lesson3.modified_lesson2.vehicles;

import com.geekhub.lesson3.modified_lesson2.VehicleFactory;
import com.geekhub.lesson3.modified_lesson2.engines.SpeedException;
import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;

public class BoatBoardComputer implements OnBoardComputer, VehicleDisplay {
    SimpleBoat simpleBoat = VehicleFactory.getSimpleBoat();

    @Override
    public void travel(int distance) throws FuelException, SpeedException {
        simpleBoat.getEngine().setDistance(distance);
        simpleBoat.accelerate();
    }

    @Override
    public void fillFuel(int amount) throws FuelException {
        simpleBoat.getGasTank().getFuel().addAmount(amount);
    }

    @Override
    public void increaseSpeed(int amount) throws SpeedException {
        simpleBoat.getEngine().increaseSpeed(amount);
    }

    @Override
    public void reduceSpeed(int amount) throws SpeedException {
        simpleBoat.getEngine().reduceSpeed(amount);
    }

    @Override
    public int getRemainingFuel() {
        return simpleBoat.getGasTank().getFuel().getAmount();
    }

    @Override
    public int getSpeed() {
        return simpleBoat.getEngine().getSpeed();
    }

    @Override
    public int getDistanceTraveledBy() throws SpeedException, FuelException {
        return simpleBoat.getEngine().getTorqueMoment().getDistanceTraveledBy();
    }
}