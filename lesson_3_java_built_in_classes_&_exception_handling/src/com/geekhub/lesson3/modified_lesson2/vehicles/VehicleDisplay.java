package com.geekhub.lesson3.modified_lesson2.vehicles;

import com.geekhub.lesson3.modified_lesson2.engines.SpeedException;
import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;

public interface VehicleDisplay {
    int getRemainingFuel();

    int getSpeed();

    int getDistanceTraveledBy() throws SpeedException, FuelException;
}