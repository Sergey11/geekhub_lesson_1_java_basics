package com.geekhub.lesson3.modified_lesson2.vehicles;

import com.geekhub.lesson3.modified_lesson2.engines.SpeedException;
import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;

public interface OnBoardComputer {
    void travel(int distance) throws FuelException, SpeedException;

    void fillFuel(int amount) throws FuelException;

    void increaseSpeed(int count) throws SpeedException;

    void reduceSpeed(int count) throws SpeedException;
}