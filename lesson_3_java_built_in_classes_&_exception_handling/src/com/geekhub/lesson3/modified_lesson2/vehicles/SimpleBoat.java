package com.geekhub.lesson3.modified_lesson2.vehicles;

import com.geekhub.lesson3.modified_lesson2.engines.Engine;
import com.geekhub.lesson3.modified_lesson2.engines.SpeedException;
import com.geekhub.lesson3.modified_lesson2.engines.TorqueMoment;
import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;
import com.geekhub.lesson3.modified_lesson2.gastanks.GasTank;
import com.geekhub.lesson3.modified_lesson2.wheels.Direction;
import com.geekhub.lesson3.modified_lesson2.wheels.Propeller;

public class SimpleBoat extends Vehicle {
    private Propeller propeller;

    public SimpleBoat(GasTank gasTank, Engine engine, Propeller propeller) {
        super(gasTank, engine);
        this.propeller = propeller;
    }

    @Override
    public void accelerate() throws FuelException, SpeedException {
        TorqueMoment torqueMoment = getEngine().getTorqueMoment();
        propeller.run(torqueMoment);
    }

    @Override
    public void brake() throws SpeedException, FuelException {
        TorqueMoment torqueMoment = getEngine().getTorqueMoment();
        propeller.stop(torqueMoment);
    }

    @Override
    public void turn(Direction direction) {
        propeller.turn(direction);
        if (direction == Direction.LEFT) {
            System.out.println("I turn left");
        } else {
            System.out.println("I turn rite");
        }
    }
}
