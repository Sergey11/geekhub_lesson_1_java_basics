package com.geekhub.lesson3.modified_lesson2.vehicles;

import com.geekhub.lesson3.modified_lesson2.engines.Engine;
import com.geekhub.lesson3.modified_lesson2.gastanks.GasTank;

public abstract class Vehicle implements Drivable {
    private GasTank gasTank;
    private Engine engine;

    protected Vehicle(GasTank gasTank, Engine engine) {
        this.gasTank = gasTank;
        this.engine = engine;
    }

    public GasTank getGasTank() {
        return gasTank;
    }

    public Engine getEngine() {
        return engine;
    }
}