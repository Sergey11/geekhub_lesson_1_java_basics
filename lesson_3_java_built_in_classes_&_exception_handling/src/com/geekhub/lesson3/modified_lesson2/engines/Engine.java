package com.geekhub.lesson3.modified_lesson2.engines;

import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;

public interface Engine {
    public TorqueMoment getTorqueMoment() throws FuelException, SpeedException;

    public void setDistance(int distance);

    public int getSpeed();

    public void reduceSpeed(int speed) throws SpeedException;

    public void increaseSpeed(int speed) throws SpeedException;
}
