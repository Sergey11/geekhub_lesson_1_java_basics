package com.geekhub.lesson3.modified_lesson2.engines;

import com.geekhub.lesson3.modified_lesson2.gastanks.Fuel;

public class SimpleBoatEngine implements Engine {
    private final int MAX_SPEED;
    private int speed = 50;
    private int distance = 100;
    private TorqueMoment torqueMoment;

    public SimpleBoatEngine(Fuel fuel) {
        MAX_SPEED = 150;
        torqueMoment = new TorqueMoment(fuel);
    }

    @Override
    public void setDistance(int distance) {
        this.distance = distance;
    }

    @Override
    public int getSpeed() {
        return speed;
    }

    @Override
    public void reduceSpeed(int speed) throws SpeedException {
        if (this.speed + speed >= MAX_SPEED) {
            this.speed = MAX_SPEED;
            throw new SpeedException("Boat traveling at a maximum speed");
        } else {
            this.speed += speed;
        }
    }

    @Override
    public void increaseSpeed(int speed) throws SpeedException {
        if (this.speed - speed <= 0) {
            this.speed = 0;
            throw new SpeedException("Boat stopped");
        } else {
            this.speed -= speed;
        }
    }

    @Override
    public TorqueMoment getTorqueMoment() {
        torqueMoment.setSpeed(speed);
        torqueMoment.setDistance(distance);
        return torqueMoment;
    }
}