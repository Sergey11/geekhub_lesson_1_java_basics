package com.geekhub.lesson3.modified_lesson2.engines;

import com.geekhub.lesson3.modified_lesson2.gastanks.Fuel;
import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;

public class TorqueMoment {
    private int speed;
    private int distance;
    private Fuel fuel;

    public TorqueMoment(Fuel fuel) {
        this.fuel = fuel;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDistanceTraveledBy() {
        return distance;
    }

    public void run() throws SpeedException, FuelException {
        if (speed > 0 && speed < 50) {
            fuel.takeAmount(15);
        } else if (speed >= 50 && speed < 100) {
            fuel.takeAmount(20);
        } else {
            fuel.takeAmount(30);
        }
    }

    public void stop() throws SpeedException {
        speed = 0;
        throw new SpeedException("vehicle stands");
    }
}