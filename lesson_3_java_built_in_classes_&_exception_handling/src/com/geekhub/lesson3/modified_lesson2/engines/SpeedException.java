package com.geekhub.lesson3.modified_lesson2.engines;

public class SpeedException extends Throwable {
    private String detail;

    SpeedException(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return detail;
    }
}