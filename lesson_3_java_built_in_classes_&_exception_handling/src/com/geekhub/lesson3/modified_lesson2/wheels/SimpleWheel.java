package com.geekhub.lesson3.modified_lesson2.wheels;

import com.geekhub.lesson3.modified_lesson2.engines.SpeedException;
import com.geekhub.lesson3.modified_lesson2.engines.TorqueMoment;
import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;

public class SimpleWheel implements Wheel {
    @Override
    public void run(TorqueMoment torqueMoment) throws SpeedException, FuelException {
        torqueMoment.run();
    }

    @Override
    public void stop(TorqueMoment torqueMoment) throws SpeedException {
        torqueMoment.stop();
    }

    @Override
    public void turn(Direction direction) {

    }
}
