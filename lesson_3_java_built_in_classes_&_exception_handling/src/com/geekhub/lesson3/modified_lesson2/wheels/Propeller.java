package com.geekhub.lesson3.modified_lesson2.wheels;

import com.geekhub.lesson3.modified_lesson2.engines.SpeedException;
import com.geekhub.lesson3.modified_lesson2.engines.TorqueMoment;
import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;

public interface Propeller {
    void run(TorqueMoment torqueMoment) throws SpeedException, FuelException;

    void stop(TorqueMoment torqueMoment) throws SpeedException;

    void turn(Direction direction);
}
