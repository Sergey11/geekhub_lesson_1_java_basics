package com.geekhub.lesson3.modified_lesson2.gastanks;

public class SimpleBoatGasTank implements GasTank{
    private Fuel fuel;

    public SimpleBoatGasTank() {
        int volume = 40;
        fuel = new Fuel(volume);
    }

    @Override
    public Fuel getFuel() {
        return fuel;
    }
}