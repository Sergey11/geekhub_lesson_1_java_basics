package com.geekhub.lesson3.modified_lesson2.gastanks;

public class Fuel {
    private final int maxAmount;
    private int amount;

    public Fuel(int maxAmount) {
        this.maxAmount = maxAmount;
        amount = maxAmount;
    }

    public int getAmount() {
        return amount;
    }

    public void addAmount(int amount) throws FuelException {
        if (this.amount + amount <= maxAmount) {
            this.amount += amount;
        } else {
            throw new FuelException("full tank");
        }
    }

    public void takeAmount(int amount) throws FuelException {
        if (this.amount - amount >= 0) {
            this.amount -= amount;
        } else {
            throw new FuelException("tank empty, refuel");
        }
    }
}
