package com.geekhub.lesson3.modified_lesson2.gastanks;

public interface GasTank {
    Fuel getFuel();
}
