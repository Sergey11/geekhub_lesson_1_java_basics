package com.geekhub.lesson3.modified_lesson2.gastanks;

/**
 * Created by sergey on 04.11.14.
 */
public class FuelException extends Throwable {
    private String detail;

    FuelException(String detail) {
        this.detail = detail;
    }

    @Override
    public String toString() {
        return detail;
    }
}
