package com.geekhub.lesson3.modified_lesson2;

import com.geekhub.lesson3.modified_lesson2.engines.SpeedException;
import com.geekhub.lesson3.modified_lesson2.gastanks.FuelException;
import com.geekhub.lesson3.modified_lesson2.vehicles.OnBoardComputer;
import com.geekhub.lesson3.modified_lesson2.vehicles.VehicleDisplay;

public class InitializationActionVehicle {
    private OnBoardComputer onBoardComputer;
    private VehicleDisplay vehicleDisplay;
    private ConsoleHandler consoleHandler = new ConsoleHandler();
    private String[] validValues = {"?", "q", "g", "f", "i", "r"};

    public InitializationActionVehicle(OnBoardComputer onBoardComputer, VehicleDisplay vehicleDisplay) {
        this.onBoardComputer = onBoardComputer;
        this.vehicleDisplay = vehicleDisplay;
        System.out.println(viewAllCommands());
    }

    public void startConsole() {
        String word = consoleHandler.checkLinesBy(validValues);
        switch (word) {
            case "?":
                System.out.println(viewAllCommands());
                break;
            case "q":
                System.exit(0);
                break;
            case "g":
                System.out.println(viewTravel());
                System.out.println(viewConditionOfTheVehicle());
                break;
            case "f":
                System.out.println(viewFillFuel());
                break;
            case "i":
                System.out.println(viewIncreaseSpeed());
                break;
            case "r":
                System.out.println(viewReduceSpeed());
                break;
        }
        System.out.println(vehicleDisplay.getRemainingFuel() + "- fuel, " + vehicleDisplay.getSpeed() + "- speed");
        startConsole();
    }

    public String viewAllCommands() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("? for view all commands,\n");
        stringBuilder.append("q for exit, \n");
        stringBuilder.append("g for travel,\n");
        stringBuilder.append("f for fill fuel,\n");
        stringBuilder.append("i for increase speed,\n");
        stringBuilder.append("r for reduce speed");
        return stringBuilder.toString();
    }

    public String viewTravel() {
        int distance = 0;
        try {
            onBoardComputer.travel(150);
            distance = vehicleDisplay.getDistanceTraveledBy();
        } catch (FuelException | SpeedException e) {
            System.out.println(e);
            startConsole();
        }
        return "you drove " + distance + "km";
    }

    public String viewConditionOfTheVehicle() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("vehicle speed ");
        stringBuilder.append(vehicleDisplay.getSpeed());
        stringBuilder.append("km, remaining fuel ");
        stringBuilder.append(vehicleDisplay.getRemainingFuel());
        stringBuilder.append("l");
        return stringBuilder.toString();
    }

    public String viewFillFuel() {
        try {
            onBoardComputer.fillFuel(10);
        } catch (FuelException e) {
            System.out.println(e);
            startConsole();
        }
        return "poured 10l, in the vehicle tank " + vehicleDisplay.getRemainingFuel() + "l";
    }

    public String viewIncreaseSpeed() {
        try {
            onBoardComputer.increaseSpeed(10);
        } catch (SpeedException e) {
            System.out.println(e);
            startConsole();
        }
        return "increase speed 10km/h, in the vehicle speed " + vehicleDisplay.getSpeed() + "km/h";
    }

    public String viewReduceSpeed() {
        try {
            onBoardComputer.reduceSpeed(10);
        } catch (SpeedException e) {
            System.out.println(e);
            startConsole();
        }
        return "increase speed 10km/h, in the vehicle speed " + vehicleDisplay.getSpeed() + "km/h";
    }
}
