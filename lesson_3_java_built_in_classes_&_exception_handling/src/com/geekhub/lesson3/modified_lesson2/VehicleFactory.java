package com.geekhub.lesson3.modified_lesson2;

import com.geekhub.lesson3.modified_lesson2.engines.Engine;
import com.geekhub.lesson3.modified_lesson2.engines.SimpleBoatEngine;
import com.geekhub.lesson3.modified_lesson2.engines.SimpleCarEngine;
import com.geekhub.lesson3.modified_lesson2.gastanks.GasTank;
import com.geekhub.lesson3.modified_lesson2.gastanks.SimpleBoatGasTank;
import com.geekhub.lesson3.modified_lesson2.gastanks.SimpleCarGasTank;
import com.geekhub.lesson3.modified_lesson2.vehicles.SimpleBoat;
import com.geekhub.lesson3.modified_lesson2.vehicles.SolarPoweredCar;
import com.geekhub.lesson3.modified_lesson2.wheels.Propeller;
import com.geekhub.lesson3.modified_lesson2.wheels.SimplePropeller;
import com.geekhub.lesson3.modified_lesson2.wheels.SimpleWheel;
import com.geekhub.lesson3.modified_lesson2.wheels.Wheel;

public class VehicleFactory {

    public static SolarPoweredCar getSolarPoweredCar() {
        GasTank simpleCarGasTank = new SimpleCarGasTank();
        Engine simpleCarEngine = new SimpleCarEngine(simpleCarGasTank.getFuel());
        Wheel[] wheels = {
                new SimpleWheel(),
                new SimpleWheel(),
                new SimpleWheel(),
                new SimpleWheel()
        };
        return new SolarPoweredCar(simpleCarGasTank, simpleCarEngine, wheels);
    }

    public static SimpleBoat getSimpleBoat() {
        GasTank simpleBoatGasTank = new SimpleBoatGasTank();
        Engine simpleCarEngine = new SimpleBoatEngine(simpleBoatGasTank.getFuel());
        Propeller propeller = new SimplePropeller();
        return new SimpleBoat(simpleBoatGasTank, simpleCarEngine, propeller);
    }
}
