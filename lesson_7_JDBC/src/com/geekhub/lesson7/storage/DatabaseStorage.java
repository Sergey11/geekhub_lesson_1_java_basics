package com.geekhub.lesson7.storage;

import com.geekhub.lesson7.objects.Entity;
import com.geekhub.lesson7.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Implementation of {@link com.geekhub.lesson7.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link com.geekhub.lesson7.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        //implement me according to interface by using extractResult method
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase();
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        //implement me
        String sql = "DELETE FROM " + entity.getClass().getSimpleName().toLowerCase() + " WHERE id = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            int result = statement.executeUpdate(sql);
            return result > 0;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        //implement me, need to save/update object and update it with new id if it's a creation
        Map<String, Object> data = prepareEntity(entity);

        if (entity.isNew()) {
            //implement me
            //need to define right SQL query to create object
            String keys = "";
            String values = "";
            for (Entry<String, Object> entry : data.entrySet()) {
                keys += entry.getKey() + ", ";
                values += "?, ";
            }
            values = values.substring(0, values.length() - 2);
            keys = keys.substring(0, keys.length() - 2);
            String sql = "INSERT INTO " + entity.getClass().getSimpleName().toLowerCase()
                    + " (" + keys + ") VALUES (" + values + ")";
            PreparedStatement stmt = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            int i = 1;
            for (Entry<String, Object> entry : data.entrySet()) {
                stmt.setObject(i, entry.getValue());
                i++;
            }
            stmt.executeUpdate();
            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    entity.setId(generatedKeys.getInt(1));
                } else {
                    throw new SQLException("Creating user failed, no ID obtained.");
                }
            }
        } else {
            //implement me
            //need to define right SQL query to update object
            String result = "";
            for (Entry<String, Object> entry : data.entrySet()) {
                result += entry.getKey() + " = ?, ";
            }
            result = result.substring(0, result.length() - 2);
            String sql = "UPDATE " + entity.getClass().getSimpleName().toLowerCase() + " SET "
                    + result + " WHERE id = " + entity.getId();
            PreparedStatement stmt = connection.prepareStatement(sql);
            int i = 1;
            for (Entry<String, Object> entry : data.entrySet()) {
                stmt.setObject(i, entry.getValue());
                i++;
            }
            stmt.executeUpdate();
        }
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        //implement me
        Map<String, Object> entityFields = new HashMap<>();
        Field[] fields = entity.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(Ignore.class)) {
                entityFields.put(field.getName(), field.get(entity));
            }
            field.setAccessible(false);
        }
        return entityFields;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        //implement me
        List<T> entities = new ArrayList<>();
        while (resultset.next()) {
            T entity = clazz.newInstance();
            Field[] fields = entity.getClass().getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true);
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.set(entity, resultset.getObject(field.getName()));
                }
                field.setAccessible(false);
            }
            Field[] fieldsSuperclass = entity.getClass().getSuperclass().getDeclaredFields();
            for (Field field : fieldsSuperclass) {
                field.setAccessible(true);
                if (!field.isAnnotationPresent(Ignore.class)) {
                    field.set(entity, resultset.getObject(field.getName()));
                }
                field.setAccessible(false);
            }
            entities.add(entity);
        }
        return entities;
    }
}
