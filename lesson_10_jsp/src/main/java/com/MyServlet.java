package com;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

@WebServlet("/user")
public class MyServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Entity> entityList = (List<Entity>) request.getSession().getAttribute("entityList");
        request.setAttribute("entities", entityList);
        RequestDispatcher rd = getServletContext().getRequestDispatcher("/new.jsp");
        rd.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Entity> entityList = (List<Entity>) request.getSession().getAttribute("entityList");
        entityList = (entityList == null) ? new ArrayList<Entity>() : entityList;

        String name = request.getParameter("name");
        String value = request.getParameter("value");
        entityList.add(new Entity(new Date().getTime(), name, value));

        request.getSession().setAttribute("entityList", entityList);
        response.sendRedirect("user");
    }
}