package com;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class EntitySort {
    public static List<Entity> sortByName(List<Entity> entityList) {
        if (entityList == null) {
            return null;
        }
        Collections.sort(entityList, new Comparator<Entity>() {
            public int compare(Entity o1, Entity o2)
            {
                return o1.getName().compareTo(o2.getName());
            }});
        return removeRepetitionName(entityList);
    }

    private static List<Entity> removeRepetitionName(List<Entity> entityList) {
        String previousName = "";
        for (Entity entity: entityList) {
            if (previousName.equals(entity.getName())) {
                entity.setName("");
            }
            previousName = (entity.getName().equals("")) ? previousName : entity.getName();
        }
        return entityList;
    }
}
