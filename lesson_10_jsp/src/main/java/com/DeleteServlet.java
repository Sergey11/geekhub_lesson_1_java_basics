package com;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/delete/*")
public class DeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Entity> entityList = (List<Entity>) request.getSession().getAttribute("entityList");
        for (int i = 0; i < entityList.size(); i++) {
            if (entityList.get(i).getId() == Long.parseLong(request.getParameter("id"))) {
                entityList.remove(i);
            }
        }
        request.getSession().setAttribute("entityList", entityList);
        response.sendRedirect("user");
    }
}
