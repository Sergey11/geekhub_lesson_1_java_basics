<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c' %>
<%@ taglib uri="/WEB-INF/tld/custom.tld" prefix="cus"%>

<html>
<head>
    <title></title>
</head>
<body>
<table>
    <tr>
        <th>Name</th>
        <th>Value</th>
        <th>Action</th>
    <tr>
        <c:forEach var="entity" items="${cus:sortByName(entities)}">
            <tr>
                <td>${entity.name}</td>
                <td>${entity.value}</td>
                <td><a href="/delete?id=${entity.id}">delete</a>
                </td>
            </tr>
        </c:forEach>
    <tr>
        <form action="user" method="post">
            <td><input type="text" name="name"></td>
            <td><input type="text" name="value"></td>
            <td colspan="2" align="center">
                <input type="submit" value="Add">
            </td>
        </form>
    </tr>
</table>
</body>
</html>