package com.geekhub.lesson1;

import java.util.HashMap;
import java.util.Map;

/**
 * This class converts a number into words different ways
 * @author Created by Musienko Sergey on 18.10.14.
 * @version 0.2
 */
public class NumbersTranslator {
    /**
     * This method converts a number into words - first method
     * @param number this natural number from 0 to 9
     * @return number as a word or error text
     */
    public String getNumberOfWordsA(int number) {
        if (number == 0) {
            return "Zero";
        } else if (number == 1) {
            return "One";
        } else if (number == 2) {
            return "Two";
        } else if (number == 3) {
            return "Three";
        } else if (number == 4) {
            return "Four";
        } else if (number == 5) {
            return "Five";
        } else if (number == 6) {
            return "Six";
        } else if (number == 7) {
            return "Seven";
        } else if (number == 8) {
            return "Eight";
        } else if (number == 9) {
            return "Nine";
        } else {
            return "Invalid number";
        }
    }

    /**
     * This method converts a number into words - second method
     * @param number this natural number from 0 to 9
     * @return number as a word or error text
     */
    public String getNumberOfWordsB(int number) {
        String word;
        switch (number) {
            case 0: word = "Zero";
                break;
            case 1: word = "One";
                break;
            case 2: word = "Two";
                break;
            case 3: word = "Three";
                break;
            case 4: word = "Four";
                break;
            case 5: word = "Five";
                break;
            case 6: word = "Six";
                break;
            case 7: word = "Seven";
                break;
            case 8: word = "Eight";
                break;
            case 9: word = "Nine";
                break;
            default:word = "Invalid number";
                break;
        }
        return word;
    }

    /**
     * This method converts a number into words - third method
     * @param number this natural number from 0 to 9
     * @return number as a word or error text
     */
    public String getNumberOfWordsC(int number) {
        String answer = null;
        Map<Integer, String> map = new HashMap<>();
        map.put(0, "Zero");
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Three");
        map.put(4, "Four");
        map.put(5, "Five");
        map.put(6, "Six");
        map.put(7, "Seven");
        map.put(8, "Eight");
        map.put(9, "Nine");

        if (number >= 0 && number <= 9) {
            answer = map.get(number);
        } if (answer == null) {
            answer = "Invalid number";
        }
        return answer;
    }
}
