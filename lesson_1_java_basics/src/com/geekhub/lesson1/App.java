package com.geekhub.lesson1;

/**
 * @author Created by Musienko Sergey on 16.10.14.
 */
public class App {
    public static void main(String[] args) {

        int number;
        Calculator calc = new Calculator();
        NumbersTranslator numTrans = new NumbersTranslator();
        ConsoleHandler conHandler = new ConsoleHandler();

        System.out.println("Insert the factorial of a non-negative integer");
        number = conHandler.checkLinesBy();

        System.out.println("Write the number 1 if you want to use the normal method of calculation, \n" +
                "Write the number 2 if you want recursive.");
        switch (conHandler.checkLinesBy(2)) {
            case 1: System.out.println(calc.factorial(number));
                break;
            case 2: System.out.println(calc.factorialRec(number));
                break;
        }

        System.out.println("Insert the positive integer of the Fibonacci sequence");
        number = conHandler.checkLinesBy();
        System.out.println(calc.fibonacci(number));

        System.out.println("Insert the natural number from 0 to 9");
        number = conHandler.checkLinesBy();

        System.out.println("write the number 1 if you want to use the first method of calculation, \n" +
                "write the number 2 if you want to use the second, \n" +
                "write the number 3 if you want to use the third.");

        switch (conHandler.checkLinesBy(3)) {
            case 1: System.out.println(numTrans.getNumberOfWordsA(number));
                break;
            case 2: System.out.println(numTrans.getNumberOfWordsB(number));
                break;
            case 3: System.out.println(numTrans.getNumberOfWordsC(number));
                break;
        }
    }
}