package com.geekhub.lesson1;

import java.util.Scanner;

/**
 * This class checks the validity of the value of the console.
 * @author Created by Musienko Sergey on 24.10.14.
 * @version 0.1
 */
public class ConsoleHandler {
    private Scanner in;

    public ConsoleHandler () {
        in = new Scanner(System.in);
    }
    /**
     * This method reads the console line and check for validity.
     * @return returns the number of the console
     */
    public int checkLinesBy() {
        String line = in.nextLine();
        try {
            return Integer.parseInt(line);
        } catch (Exception e) {
            System.out.println("You write not a number \"" + line + "\"");
            System.out.println("Try again");
            return checkLinesBy();
        }
    }

    /**
     * This method reads the console line and check for validity.
     * @param maxValue the maximum value of the return of
     * @return returns the number of the console
     */
    public int checkLinesBy(int maxValue) {
        int value = checkLinesBy();
        if (maxValue >= value && value > 0) {
            return value;
        } else {
            System.out.println("Try again");
            return checkLinesBy(maxValue);
        }
    }
}