package com.geekhub.lesson1;

/**
 * This class calculates the number
 * @author Created by Musienko Sergey on 16.10.14.
 * @version 0.4
 */
public class Calculator {
    /**
     * This method calculates the Fibonacci sequence
     * @param number this number indicates how many numbers
     *               in the Fibonacci sequence output
     * @return returns the sequence of Fibonacci or error text
     */
    public String fibonacci(int number) {
        int f0 = 0;
        int f1 = 1;
        int fn;
        String result;

        if (number >= 0) {
            result = "F" + number + " = " + f0;
            if (number >= 1) {
                result += ", " + f1;
                for (int i = 0; i < (number - 1); i++) {
                    fn = f0 + f1;
                    f0 = f1;
                    f1 = fn;
                    result += ", " + fn;
                }
            }
            return result + ";";
        } else {
            return "Invalid value";
        }
    }

    /**
     * This method calculates factorial of a number
     * @param number this number is factorial of a non-negative integer
     * @return the result of the factorial or error text
     */
    public String factorial(int number) {
        int a = 1;
        for (int i = 0; i < number; i++) {
            a *= (i + 1);
        } if (number >= 0) {
            return number + "! = " + a + ";";
        } else {
            return "Invalid value";
        }
    }
    /**
     * This method calculates factorial of a recursive method
     * @param number this number is factorial of a non-negative integer
     * @return the result of the factorial or error text
     */
    public String factorialRec(int number) {
        return (number >= 0) ? number + "! = " + factorialDoneRecursion(number) + ";" : "Invalid value";
    }

    private int factorialDoneRecursion(int a) {
        return (a > 1) ? a * factorialDoneRecursion(a - 1) : 1;
    }
}
