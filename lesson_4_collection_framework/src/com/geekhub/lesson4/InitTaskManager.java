package com.geekhub.lesson4;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class InitTaskManager {
    TaskManager taskManager = new SimpleTaskManager();
    ConsoleHandler consoleHandler = new ConsoleHandler();

    public void initTask() {
        System.out.println("Enter Category");
        String category = consoleHandler.checkLinesBy();
        System.out.println("Enter Description");
        String description = consoleHandler.checkLinesBy();
        Date date = new Date(System.currentTimeMillis());
        taskManager.addTask(date, new Task(category, description));
        System.out.println(date);
    }

    public void initRemoveTask() {
        System.out.println("Enter date remove task");
        String stringDate = consoleHandler.checkLinesBy();
        SimpleDateFormat format = new SimpleDateFormat();
        try {
            taskManager.removeTask(format.parse(stringDate));
        } catch (ParseException ex) {
            System.out.println("Try again");
            initRemoveTask();
        }
    }

    public void initCategories() {
        System.out.println("All categories");
        for (String category : taskManager.getCategories()) {
            System.out.println(category);
        }
    }

    public void initTasksByCategories() {
        System.out.println("All Task by Categories");
        for (Map.Entry<String, List<Task>> entry : taskManager.getTasksByCategories().entrySet()) {
            for (Task task : entry.getValue()) {
                System.out.println("Category " + task.getCategory());
                System.out.println("Description" + task.getDescription());
            }
        }
    }

    public void initTasksByCategory() {
        System.out.println("Enter Category");
        String category = consoleHandler.checkLinesBy();
        System.out.println("All Task by Category " + category);
        for (Task task : taskManager.getTasksByCategory(category)) {
            System.out.println("Category " + task.getCategory());
            System.out.println("Description" + task.getDescription());
        }
    }

    public void initTasksForToday() {
        System.out.println("Tasks for today");
        for (Task task : taskManager.getTasksForToday()) {
            System.out.println("Category " + task.getCategory());
            System.out.println("Description" + task.getDescription());
        }
    }
}