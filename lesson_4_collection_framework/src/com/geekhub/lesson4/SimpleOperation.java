package com.geekhub.lesson4;

import java.util.HashSet;
import java.util.Set;

public class SimpleOperation implements SetOperations {

    @Override
    public boolean equals(Set a, Set b) {
        return a.size() == b.size() && a.containsAll(b);
    }

    @Override
    public Set union(Set a, Set b) {
        Set c = new HashSet();
        c.addAll(a);
        c.addAll(b);
        return c;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set c = new HashSet();
        c.addAll(a);
        c.removeAll(b);
        return c;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set c = new HashSet();
        c.addAll(a);
        c.retainAll(b);
        return c;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        Set c = union(a, b);
        c.removeAll(intersect(a, b));
        return c;
    }
}
