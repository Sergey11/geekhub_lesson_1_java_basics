package com.geekhub.lesson4;

import java.util.Scanner;

public class ConsoleHandler {
    private Scanner in;

    public ConsoleHandler() {
        in = new Scanner(System.in);
    }

    public String checkLinesBy(String[] validValues) {
        String line = in.nextLine();
        for (String validValue : validValues) {
            if (validValue.equals(line)) {
                return line;
            }
        }
        System.out.println("Try again");
        return checkLinesBy(validValues);
    }

    public String checkLinesBy() {
        return in.nextLine();
    }
}