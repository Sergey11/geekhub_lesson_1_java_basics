package com.geekhub.lesson4;

import java.text.SimpleDateFormat;
import java.util.*;

public class SimpleTaskManager implements TaskManager {

    private Map<Date, Task> map = new TreeMap<>();

    @Override
    public void addTask(Date date, Task task) {
        map.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        map.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        return getTasksByCategories().keySet();
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> tasksByCategories = new HashMap<>();
        for (Map.Entry<Date, Task> entry : map.entrySet()) {
            if (!tasksByCategories.containsKey(entry.getValue().getCategory())) {
                tasksByCategories.put(entry.getValue().getCategory(), new ArrayList<>());
            }
            tasksByCategories.get(entry.getValue().getCategory()).add(entry.getValue());
        }
        return tasksByCategories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        return getTasksByCategories().get(category);
    }

    @Override
    public List<Task> getTasksForToday() {
        List<Task> tasks = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
        String dateToday = dateFormat.format(System.currentTimeMillis());
        for (Map.Entry<Date, Task> entry : map.entrySet()) {
            if (dateFormat.format(entry.getKey()).equals(dateToday)) {
                tasks.add(entry.getValue());
            }
        }
        return tasks;
    }
}
