package com.geekhub.lesson4;

import java.util.HashSet;
import java.util.Set;

public class InitOperation {

    SetOperations setOperations = new SimpleOperation();
    private ConsoleHandler consoleHandler = new ConsoleHandler();

    public Set<String> createdSet() {
        Set<String> set = new HashSet<>();
        for (int i = 0; i < 4; i++) {
            System.out.println("Enter " + (i + 1) + " name");
            set.add(consoleHandler.checkLinesBy());
        }
        return set;
    }

    public void initEquals(Set first, Set second) {
        if (setOperations.equals(first, second)) {
            System.out.println("First collection equals second collection");
        } else {
            System.out.println("First collection is not equals second collection");
        }
    }

    public void initUnion(Set first, Set second) {
        System.out.println("Union collections");
        for (Object text : setOperations.union(first, second)) {
            System.out.println((String) text);
        }
    }

    public void initSubtract(Set first, Set second) {
        System.out.println("Subtract collections");
        for (Object text : setOperations.subtract(first, second)) {
            System.out.println((String) text);
        }
    }

    public void initIntersect(Set first, Set second) {
        System.out.println("Intersect collections");
        for (Object text : setOperations.intersect(first, second)) {
            System.out.println((String) text);
        }
    }

    public void initSymmetricSubtract(Set first, Set second) {
        System.out.println("Symmetric Subtract collections");
        for (Object text : setOperations.intersect(first, second)) {
            System.out.println((String) text);
        }
    }
}
