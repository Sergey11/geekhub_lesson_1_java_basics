package com.geekhub.lesson4;

import java.util.Set;

public class Application {
    public static InitTaskManager initTaskManager = new InitTaskManager();

    public static void main(String[] args) {
        InitOperation initOperation = new InitOperation();
        System.out.println("Create first name collection");
        Set first = initOperation.createdSet();
        System.out.println("Create second name collection");
        Set second = initOperation.createdSet();
        initOperation.initEquals(first, second);
        initOperation.initUnion(first, second);
        initOperation.initSubtract(first, second);
        initOperation.initIntersect(first, second);
        initOperation.initSymmetricSubtract(first, second);

        String[] validLetters = {"?", "q", "a", "r", "c", "h", "f", "t"};
        System.out.println(viewAllCommands());
        selectionOfWork(validLetters);
    }

    public static String viewAllCommands() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("? - view all commands,\n");
        stringBuilder.append("q - exit, \n");
        stringBuilder.append("a - create task,\n");
        stringBuilder.append("r - remove task, \n");
        stringBuilder.append("c - view category, \n");
        stringBuilder.append("h - view tasks by categories, \n");
        stringBuilder.append("f - view tasks by category, \n");
        stringBuilder.append("t - view tasks for today, \n");
        return stringBuilder.toString();
    }

    public static void selectionOfWork(String[] letters) {
        ConsoleHandler consoleHandler = new ConsoleHandler();
        switch (consoleHandler.checkLinesBy(letters)) {
            case "q":
                System.exit(0);
                break;
            case "a":
                initTaskManager.initTask();
                break;
            case "r":
                initTaskManager.initRemoveTask();
                break;
            case "c":
                initTaskManager.initCategories();
                break;
            case "h":
                initTaskManager.initTasksByCategories();
                break;
            case "f":
                initTaskManager.initTasksByCategory();
                break;
            case "t":
                initTaskManager.initTasksForToday();
                break;
            case "?":
                viewAllCommands();
                break;
        }
        selectionOfWork(letters);
    }
}
