package com.geekhub.lesson2.vehicles;

import com.geekhub.lesson2.engines.TorqueMoment;
import com.geekhub.lesson2.gastanks.Fuel;
import com.geekhub.lesson2.wheels.Direction;
import com.geekhub.lesson2.engines.Engine;
import com.geekhub.lesson2.gastanks.GasTank;
import com.geekhub.lesson2.wheels.Propeller;

public class SimpleBoat extends Vehicle {
    private Propeller propeller;

    public SimpleBoat(GasTank gasTank, Engine engine, Propeller propeller) {
        super(gasTank, engine);
        this.propeller = propeller;
        System.out.println("Hello, I am a Boat");
    }

    @Override
    public void accelerate() {
        Fuel fuel = getGasTank().getFuel();
        TorqueMoment torqueMoment = getEngine().getTorqueMoment(fuel);
            propeller.run(torqueMoment);
        System.out.println("I accelerated");
    }

    @Override
    public void brake() {
        TorqueMoment torqueMoment = getEngine().stopEngine();
            propeller.stop(torqueMoment);
        System.out.println("I braked");
    }

    @Override
    public void turn(Direction direction) {
        propeller.turn(direction);
        if (direction == Direction.LEFT) {
            System.out.println("I turn left");
        } else {
            System.out.println("I turn rite");
        }
    }
}
