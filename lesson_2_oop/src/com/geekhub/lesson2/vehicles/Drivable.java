package com.geekhub.lesson2.vehicles;

import com.geekhub.lesson2.wheels.Direction;

public interface Drivable {
    void accelerate();
    void brake();
    void turn(Direction direction);
}
