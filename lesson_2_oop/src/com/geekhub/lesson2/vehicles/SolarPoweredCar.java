package com.geekhub.lesson2.vehicles;

import com.geekhub.lesson2.wheels.Direction;
import com.geekhub.lesson2.gastanks.Fuel;
import com.geekhub.lesson2.engines.TorqueMoment;
import com.geekhub.lesson2.engines.Engine;
import com.geekhub.lesson2.gastanks.GasTank;
import com.geekhub.lesson2.wheels.Wheel;

public class SolarPoweredCar extends Vehicle {
    private Wheel[] wheels;

    public SolarPoweredCar(GasTank gasTank, Engine engine, Wheel[] wheels) {
        super(gasTank, engine);
        this.wheels = wheels;
        System.out.println("Hello, I am a Solar-powered car");
    }

    @Override
    public void accelerate() {
        Fuel fuel = getGasTank().getFuel();
        TorqueMoment torqueMoment = getEngine().getTorqueMoment(fuel);
        for (Wheel wheel : wheels) {
            wheel.run(torqueMoment);
        }
        System.out.println("I accelerated");
    }

    @Override
    public void brake() {
        TorqueMoment torqueMoment = getEngine().stopEngine();
        for (Wheel wheel : wheels) {
            wheel.stop(torqueMoment);
        }
        System.out.println("I braked");
    }

    @Override
    public void turn(Direction direction) {
        wheels[0].turn(direction);
        wheels[1].turn(direction);
        if (direction == Direction.LEFT) {
            System.out.println("I turn left");
        } else {
            System.out.println("I turn rite");
        }
    }

}
