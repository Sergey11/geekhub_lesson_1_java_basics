package com.geekhub.lesson2;

import java.util.Scanner;

/**
 * This class checks the validity of the value of the console.
 *
 * @author Created by Musienko Sergey on 24.10.14.
 */
public class ConsoleHandler {
    private Scanner in;

    public ConsoleHandler() {
        in = new Scanner(System.in);
    }

    /**
     * This method reads the console line and check for validity.
     *
     * @param validValues array of valid values
     * @return returns the number of the console
     */
    public int checkLinesBy(int[] validValues) {
        String line = in.nextLine();
        if (line.equals("q")) {
            return -1;
        }
        try {
            int value = Integer.parseInt(line);
            for (int validValue : validValues) {
                if (validValue == value) {
                    return value;
                }
            }
            System.out.println("Try again");
            return checkLinesBy(validValues);
        } catch (Exception e) {
            System.out.println("You write not a number \"" + line + "\"");
            System.out.println("Try again");
            return checkLinesBy(validValues);
        }
    }
}