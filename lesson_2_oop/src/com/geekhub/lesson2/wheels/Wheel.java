package com.geekhub.lesson2.wheels;

import com.geekhub.lesson2.engines.TorqueMoment;

public interface Wheel {
    void run(TorqueMoment torqueMoment);
    void stop(TorqueMoment torqueMoment);
    void turn(Direction direction);
}
