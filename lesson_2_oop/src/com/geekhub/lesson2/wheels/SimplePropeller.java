package com.geekhub.lesson2.wheels;

import com.geekhub.lesson2.engines.TorqueMoment;

public class SimplePropeller implements Propeller{

    @Override
    public void run(TorqueMoment torqueMoment) {

    }

    @Override
    public void stop(TorqueMoment torqueMoment) {

    }

    @Override
    public void turn(Direction direction) {

    }
}
