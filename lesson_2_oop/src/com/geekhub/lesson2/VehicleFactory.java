package com.geekhub.lesson2;

import com.geekhub.lesson2.engines.Engine;
import com.geekhub.lesson2.engines.SimpleBoatEngine;
import com.geekhub.lesson2.engines.SimpleCarEngine;
import com.geekhub.lesson2.gastanks.GasTank;
import com.geekhub.lesson2.gastanks.SimpleBoatGasTank;
import com.geekhub.lesson2.gastanks.SimpleCarGasTank;
import com.geekhub.lesson2.vehicles.SimpleBoat;
import com.geekhub.lesson2.vehicles.SolarPoweredCar;
import com.geekhub.lesson2.wheels.Propeller;
import com.geekhub.lesson2.wheels.SimplePropeller;
import com.geekhub.lesson2.wheels.SimpleWheel;
import com.geekhub.lesson2.wheels.Wheel;

public class VehicleFactory {

    public SolarPoweredCar getSolarPoweredCar() {
        GasTank simpleCarGasTank = new SimpleCarGasTank();
        Engine simpleCarEngine = new SimpleCarEngine();
        Wheel[] wheels = {
                new SimpleWheel(),
                new SimpleWheel(),
                new SimpleWheel(),
                new SimpleWheel()
        };
        return new SolarPoweredCar(simpleCarGasTank, simpleCarEngine, wheels);
    }

    public SimpleBoat getSimpleBoat() {
        GasTank simpleCarGasTank = new SimpleBoatGasTank();
        Engine simpleCarEngine = new SimpleBoatEngine();
        Propeller propeller = new SimplePropeller();
        return new SimpleBoat(simpleCarGasTank, simpleCarEngine, propeller);
    }
}
