package com.geekhub.lesson2.gastanks;

public class SimpleCarGasTank implements GasTank {
    @Override
    public Fuel getFuel() {
        return new Fuel();
    }
}
