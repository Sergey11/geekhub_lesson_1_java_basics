package com.geekhub.lesson2.gastanks;

public interface GasTank {
    Fuel getFuel();
}
