package com.geekhub.lesson2;

import com.geekhub.lesson2.vehicles.Drivable;
import com.geekhub.lesson2.wheels.Direction;

public class AppRunner {

    public static void main(String[] args) {
        initVehicle();
    }

    private static void initVehicle() {
        int[] numbersVehicle = {1, 2};
        VehicleFactory factory = new VehicleFactory();
        ConsoleHandler consoleHandler = new ConsoleHandler();
        System.out.println("Enter \"q\" if you want quit");
        System.out.println("Enter \"1\" if you want choose a solar-powder car, " +
                "\"2\" if you want choose a boat.");
        switch (consoleHandler.checkLinesBy(numbersVehicle)) {
            case -1:
                return;
            case 1:
                initVehicleAction(consoleHandler, factory.getSolarPoweredCar());
                break;
            case 2:
                initVehicleAction(consoleHandler, factory.getSimpleBoat());
                break;
        }
    }

    private static void initVehicleAction(ConsoleHandler consoleHandler, Drivable drivable) {
        int[] numbersAction = {1, 2, 3, 5};
        System.out.println("Enter \"5\" if you want accelerate, " +
                "\"2\" if you want brake,\n" +
                "\"1\" if you want turn left, " +
                "\"3\" if you want turn right.");
        while (true) {
            switch (consoleHandler.checkLinesBy(numbersAction)) {
                case -1:
                    return;
                case 1:
                    drivable.turn(Direction.LEFT);
                    break;
                case 2:
                    drivable.brake();
                    break;
                case 3:
                    drivable.turn(Direction.RIGHT);
                    break;
                case 5:
                    drivable.accelerate();
                    break;

            }
        }
    }
}
