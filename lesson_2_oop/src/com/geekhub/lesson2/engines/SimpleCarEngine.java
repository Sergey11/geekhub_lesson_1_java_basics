package com.geekhub.lesson2.engines;

import com.geekhub.lesson2.gastanks.Fuel;

public class SimpleCarEngine implements Engine {
    @Override
    public TorqueMoment getTorqueMoment(Fuel fuel) {
        return new TorqueMoment();
    }

    @Override
    public TorqueMoment stopEngine() {
        return new TorqueMoment();
    }
}