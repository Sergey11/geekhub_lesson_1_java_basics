package com.geekhub.lesson2.engines;

import com.geekhub.lesson2.gastanks.Fuel;

public interface Engine {
    TorqueMoment getTorqueMoment(Fuel fuel);
    TorqueMoment stopEngine();
}
